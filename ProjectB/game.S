#include "system.h"
#include "game.h"


.globl time
.globl score
.globl highscore
.globl collision
.globl score_reset

.data
    g_score: .word 0
    s_score: .word 0
    g_high_score: .word 0 
.text

collision:
    lw $t1, 0($a0)
    lw $t2, 4($a0)
    li $t0, 76
    slt $t0, $t0, $t1
    beqz $t0, .else_if_1

.go_right:
    li $t0, 2
    sw $t0, 0($a0)

.else_if_1:
    li $t0, 2
    slt $t0, $t1, $t0
    beqz $t0, .else_if_2
    
.go_left:
    li $t0, 76
    sw $t0, 0($a0)

.else_if_2:
    li $t0, 27
    slt $t0, $t0, $t2
    beqz $t0, .else_if_3

.go_up:
    li $t0, 2
    sw $t0, 4($a0)

.else_if_3:
    li $t0, 2
    slt $t0, $t2, $t0
    beqz $t0, .collision_end

.go_down:
    li $t0, 27
    sw $t0, 4($a0)

.collision_end:
    jr $ra

score:
    lui $t0, %hi(g_score)
    lw $t1, %lo(g_score)($t0)

    lui $t2, %hi(s_score)
    lw $t3, %lo(s_score)($t3)

    slt $t0, $t3, $t1
    beqz $t0, .scoreing
    addi $v0, $t1, 10
    addi $v1, $t3, 10
    sw $v0, g_score
    sw $v1, s_score
    jr $ra

.scoreing:
    addi $v0, $t1, 10
    sw $v0, g_score
    jr $ra

highscore:
    lui $t0, %hi(g_score)
    lw $t1, %lo(g_score)($t0)

    lui $t2, %hi(g_high_score)
    lw $t3, %lo(g_high_score)($t2)

    slt $t0, $t3, $t1
    beqz $t0, .highscore_quit

    lw $v0, s_score 
    addi $v0, $v0, 10
    sw $v0, g_high_score
    jr $ra
     
.highscore_quit:
    addi $v0, $t3, 0
    sw $v0, g_high_score
    jr $ra

score_reset:
    li $t0, 0
    sw $t0, g_score

ms_time:
    lui $t0, %hi(MS_COUNTER_REG_ADDR)
    lw $t1, %lo(MS_COUNTER_REG_ADDR)($t0)
