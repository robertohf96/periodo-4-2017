#ifndef SNAKE_H
#define SNAKE_H

#include "system.h"
#include "apple.h"

struct snake{
	uint32_t x;
	uint32_t y;
	uint32_t xspeed;
	uint32_t yspeed;
	uint32_t total;
	uint32_t score;
	uint32_t highscore;
	uint32_t length;
	char dir;
	char snake_h[3];
};

void snake_init(struct snake *snake);
void snake_draw(struct snake *snake);
void snake_direction(struct snake *snake, uint32_t x, uint32_t y);
void snake_update(struct snake *snake, struct apple *apple);
bool snake_eat(struct snake *snake, struct apple apple);
bool snake_collision(struct snake *snake, struct apple *apple);
void move(struct snake *snake);

#endif /*  SNAKE_H */