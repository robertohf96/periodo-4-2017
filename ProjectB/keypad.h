#ifndef KEYPAD_H
#define KEYPAD_H

#include "system.h"

#ifndef __ASSEMBLER__

void delay_ms(uint32_t ms);
void keypad_init();
uint8_t keypad_getkey();

#endif

#endif /* KEYPAD_H */