#include "apple.h"
#include "screen.h"

void apple_init(struct apple *apple)
{
	apple->x = 38;
	apple->y = 14;
	apple->head[0] = '#';
	apple->head[1] = '$';
	apple->head[2] = '\0';
}

void apple_show(struct apple apple)
{
	set_color(RED,BLACK);
	set_cursor(apple.y, apple.x);
	puts(apple.head);	
}