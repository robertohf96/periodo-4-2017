#include "screen.h"

uint16_t chr_attr;
uint cursor_col, cursor_row;
uint next_key_time;

void clear_screen()
{
    uint16_t data = (chr_attr<<8) | ' ';
    uint16_t right_side = 238;
    uint16_t left_side = 161;

    for(uint16_t i = VGA_START_ADDR; i <= VGA_END_ADDR; i=i+2){
        uint16_t *vgaptr = i;
        *vgaptr = data;

        if(i==VGA_START_ADDR+81){
            data = (chr_attr<<8) | 201;
            *vgaptr = data;
        }
        else if(i==VGA_START_ADDR+2241){
            data = (chr_attr<<8) | 200;
            *vgaptr = data;
        }
        else if(i==VGA_START_ADDR+2318){
            data = (chr_attr<<8) | 188;
            *vgaptr = data;
        }
        else if(i==VGA_START_ADDR+158){
            data = (chr_attr<<8) | 187;
            *vgaptr = data;
        }
        else if(i==(VGA_START_ADDR+left_side) && i<VGA_END_ADDR-160){
            data = (chr_attr<<8) | 186;
            *vgaptr = data;
            left_side = left_side+80;
        }
        else if(i==(VGA_START_ADDR+right_side) && i<VGA_END_ADDR-160){
            data = (chr_attr<<8) | 186;
            *vgaptr = data;
            right_side = right_side+80;
        }
        else if(i>VGA_START_ADDR+80 && i<VGA_START_ADDR+158){
            data = (chr_attr<<8) | 205;
            *vgaptr = data;
        }
        else if(i>VGA_START_ADDR+2241 && i<=VGA_START_ADDR+2317){
            data = (chr_attr<<8) | 206;
            *vgaptr = data;
        }

        data = (chr_attr<<8) | ' ';
    }
    set_cursor(0,26);
    puts("SCORE: ");
    set_cursor(0,40);
    puts("HIGHSCORE: ");
} 

void set_color(uint8_t fgcolor, uint8_t bgcolor)
{
    chr_attr = (bgcolor << 4) | (fgcolor & 0x0f);
}

void get_color(uint8_t *fgcolor, uint8_t *bgcolor)
{
    *fgcolor= chr_attr & 0x0f;
    *bgcolor = chr_attr << 4;
}

void get_cursor(uint8_t *row, uint8_t *col)
{
    *row = cursor_row;
    *col = cursor_col;
}

void set_cursor(uint row, uint col)
{
    if(row > (MAX_ROWS - 1) || col > (MAX_COLS - 1))
        return;

    cursor_row = row;
    cursor_col = col;
}

void put_char(uint8_t ch)
{
    if(ch == '\n'){
        if(cursor_row < (MAX_ROWS - 1))
            cursor_row++;

        cursor_col = 0;
    }
    else{

        uint16_t data = chr_attr << 8 | ch;
        uint16_t *vgaptr = (uint16_t *)(VGA_START_ADDR + ((uint16_t)80*cursor_row+cursor_col));

        *vgaptr = data;

        if(cursor_col < (MAX_COLS - 1)){
            cursor_col++;
        }
        else{
            cursor_col = 0;
            cursor_row++;
        }
    }
}

void puts(char *str)
{
    const char *p = str;
    
    while(*p != '\0'){
        put_char(*p++);
    }
}

void put_udecimal(uint n)
{
    char buffer[12];

    if(n==0)
        put_char('0');
    else{
        char *p = &buffer[11];
        uint quot, rem;

        *p-- = '\0';
        while(n>0){
            divide(n,10,&quot,&rem);

            *p-- = rem + '0';
            n = quot;
        }
        p++;

        puts(p);
    }
}

void divide(uint32_t dividend, uint32_t divisor, uint32_t *quotient, uint32_t *reminder)
{
    *quotient = 0;
    while(dividend >= divisor){
        dividend -= divisor;
        (*quotient) ++;
    }
    *reminder = dividend;
}