#include "snake.h"
#include "screen.h"
#include "keypad.h"

void snake_init(struct snake *snake)
{
	snake->x = 10;
	snake->y = 14;
	snake->xspeed = 2;
	snake->yspeed = 0;

	snake->dir = 'r';

	snake->total = 1;
	snake->length = 0;
	
	snake->score = 0;

	snake->snake_h[0] = '(';
	snake->snake_h[1] = ')';
	snake->snake_h[2] = '\0';

}

void snake_draw(struct snake *snake)
{	
	set_color(WHITE,BLACK);
		
	for(int i=1; i<snake->total; i++){
		set_cursor(snake[i].y, snake[i].x);
		puts(snake->snake_h);
	}	

	set_cursor(snake[0].y, snake[0].x);
	puts(snake[0].snake_h);
}

void snake_direction(struct snake *snake, uint32_t x, uint32_t y)
{
	snake->xspeed = x;
	snake->yspeed = y;
}

void snake_update(struct snake *snake, struct apple *apple)
{

	for(int i=1; i < snake[0].total-1; i++){
		snake[i].x = snake[i+1].x;
		snake[i].y = snake[i+1].y;
	}

	snake[snake->total-1].x = snake[0].x;
	snake[snake->total-1].y = snake[0].y;
	

	snake[0].x = snake[0].x + snake->xspeed;
	snake[0].y = snake[0].y + snake->yspeed;

	snake_collision(snake,apple);

}

bool snake_eat(struct snake *snake, struct apple apple)
{
	
	if(snake[0].x == apple.x && snake[0].y == apple.y)
        return true;
    else   
        return false;
}

bool snake_collision(struct snake *snake, struct apple *apple)
{

	for(int i=1; i < snake[0].length; i++){
		uint32_t posx = snake[i].x;
		uint32_t posy = snake[i].y;
		if(posx == snake[0].x && posy == snake[0].y){
			snake_init(snake);
			apple_init(apple);
			score_reset();
		}
	}
}

void move(struct snake *snake)
{

	if(keypad_getkey() == 0x01){
		if(snake[0].dir != 'r'){
			snake_direction(snake, -2, 0);
			snake[0].dir = 'l';
		}
	}
	else if(keypad_getkey() == 0x02){
		if(snake[0].dir != 'l'){
			snake_direction(snake, 2, 0);
			snake[0].dir = 'r';
		}
	}
	else if(keypad_getkey() == 0x04){
		if(snake[0].dir != 'u'){
			snake_direction(snake, 0, 1);
			snake[0].dir = 'd';
		}
	}
	else if(keypad_getkey() == 0x08){
		if(snake[0].dir != 'd'){
			snake_direction(snake, 0, -1);
			snake[0].dir = 'u';
		}
	}
	else{
		snake[0].dir = snake[0].dir;
	}
}