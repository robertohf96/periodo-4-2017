#ifndef SCREEN_H
#define SCREEN_H

#include "system.h"

void set_color(uint8_t fgcolor, uint8_t bgcolor);
void get_color(uint8_t *fgcolor, uint8_t *bgcolor);
void get_cursor(uint8_t *row, uint8_t *col);
void set_cursor(uint row, uint col);
void put_char(uint8_t ch);
void puts(char *s);
void clear_screen();
void put_udecimal(uint n);
void divide(uint32_t divided, uint32_t divisor, uint32_t *quotioent, uint32_t *reminder);

#define MAX_ROWS 	30
#define MAX_COLS 	80

#define BLUE		0x1
#define GREEN		0x2
#define CYAN		0x3
#define RED			0x4
#define GREY		0x6
#define LIGHT_BLUE	0x9
#define BLACK       0x0
#define WHITE       0xf


#endif /* SCREEN_H */