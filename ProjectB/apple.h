#ifndef APPLE_H
#define APPLE_H

#include "system.h"

struct apple{
	uint32_t x;
	uint32_t y;
	char head[3];
};

void apple_init(struct apple *apple);
void apple_show(struct apple apple);
void apple_update(struct apple *apple);



#endif /*  APPLE_H */