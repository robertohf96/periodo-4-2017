#ifndef GAME_H
#define GAME_H

#include "system.h"

#ifndef __ASSEMBLER__
    uint32_t score();
    uint32_t highscore();
    void time();
    void collision(struct snake *snake);
    void score_reset();
#else

#endif

#endif /* SCREEN_H */