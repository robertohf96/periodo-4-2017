#include "screen.h"
#include "keypad.h"
#include "snake.h"
#include "apple.h"
#include "game.h"	

struct snake snake[100];
struct apple apple;

static uint32_t random_number = 1;
//32,7,5,3,2,1
#define MASK 0x80000057

void random_seed(uint32_t seed);
uint16_t random(void);

uint32_t volatile *ptr = 0x0a8;

uint32_t i = 10;

int main()
{
	set_color(WHITE,BLACK);
	clear_screen();

	keypad_init();
	apple_init(&apple);
	snake_init(snake);

	while(1){
		apple_show(apple);

		random_seed(i);

		snake_draw(snake);
		snake_update(snake, &apple);

		collision(snake);

		set_cursor(0,32);
		put_udecimal(snake[0].score);
		set_cursor(0,50);
		put_udecimal(snake[0].highscore);

		if(snake_eat(snake,apple)){
			snake[0].total++;
			snake[0].length++;
			snake[0].score = score();
			snake[0].highscore = highscore();
			apple.x = (random());
			*ptr = apple.x;
		}
				
		move(snake);

		delay_ms(105);		
		clear_screen();

		i+=10;
	}

	return 0;
}


void random_seed(uint32_t seed)
{
 if (seed == 0)
 {
  random_number = 1;
 }
 else
 {
  random_number = seed;
 }
}

uint16_t random(void)
{
  
	for (uint16_t i = 0; i < 8; ++i)
		   {
			  if ( random_number & 0x00000001)
			  {
				   random_number = (((random_number ^ MASK) >> 1) | 0x80000000);
			  }
			  else
			  {
					random_number >>=1;
			  }
		   }
	return	((uint8_t) random_number);
}