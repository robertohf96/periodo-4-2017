#include "screen.h"

int main()
{
	clear_screen();

	char c;
	int i=0;

	for(c = 'a'; c <= 'z'; ++c){
		set_cursor(0,i);
		set_color(0x00,0xf0);
		put_char(c);
		i++;
	}
	set_cursor(5,0);
	set_color(0xf0,0x00);
	puts("Org. de Computadoras");

	for(int j=0; j<10; j++){
		set_cursor(10,j);
		set_color(0xe0,0x00);
		put_decimal(j);
	}
	return 0;
}