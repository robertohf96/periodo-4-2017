#include "screen.h"

uint8_t _cursor_row, _cursos_column;
uint8_t background_color, foreground_color;

void clear_screen()
{
	for(uint16_t i = VGA_START_ADDR ; i <= VGA_END_ADDR; i++){
		uint16_t data = 0xcf00 | ' ';
		uint16_t *vgaptr = (uint16_t *)i;
		*vgaptr = data;
	}
	set_cursor(0,0);
}

void set_cursor(uint8_t row, uint8_t column)
{
	_cursor_row = row;
	_cursos_column = column;
}

void get_cursor(uint8_t *row, uint8_t *column)
{
	*row = _cursor_row;
	*column = _cursos_column;
}

void set_color(uint8_t fgcolor, uint8_t bgcolor)
{
	background_color = bgcolor;
	foreground_color = fgcolor;
}

void get_color(uint8_t *fgcolor, uint8_t *bgcolor)
{
	*fgcolor = foreground_color;
	*bgcolor = background_color;	
}

void put_char(uint8_t ch)
{
	uint8_t fg, bg;
	get_color(&fg,&bg);
	uint8_t combine = bg | (fg >> 4);
	uint16_t fgbg = (combine << 8) | 0;
	uint16_t data = fgbg | ch;
	uint16_t *vgaptr = (uint16_t *)(VGA_START_ADDR + ((uint16_t)80*_cursor_row+_cursos_column));
	*vgaptr = data;
}

void puts(char *str)
{
	int size = 0;
	for(int i = 0; str[i] != '\0'; i++){
		size++;
	}
	for(int i = 0; i < size; i++){
		put_char(str[i]);
		if(_cursos_column < 80){
			_cursos_column = _cursos_column + 1;
		}
		else if(_cursos_column >= 80){
			_cursor_row = _cursor_row + 1;
		}
	}
}

void put_decimal(uint32_t n)
{
	if(n < 10){
		char x = (char)"0123456789"[n];
		put_char(x);
	}
	else{
		char y  = (char)"0123456789"[n];

		put_char(y); 
	}
}
